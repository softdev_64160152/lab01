/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.arisa.lab1;

import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class Lab1 {

    public static void main(String[] args) {
        System.out.println("Welcome to ox Program");
        int n = 3;
        char[][] table = new char[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                table[i][j] = '-';
            }
        }
        Scanner in = new Scanner(System.in);
        boolean p1 = true;
        boolean gameEnd = false;
        while (!gameEnd) {
            drawtable(table);

            if (p1) {
                System.out.println("x turn");
            } else {
                System.out.println("o turn");
            }

            char c = '-';
            if (p1) {
                c = 'x';
            } else {
                c = 'o';
            }

            int row;
            int col;
            

            while (true) {
                System.out.print("Please input row,col : ");
                row = in.nextInt() - 1;
                col = in.nextInt() - 1;

                if (row < 0 || col < 0 || row >= n || col >= n) {
                    System.out.println("This position is off the bounds of the board! Try again. ");
                } else if (table[row][col] != '-') {
                    System.out.println("Someone has already made a move at this position! Try again. ");
                } else {
                    break;
                }
            }
            table[row][col] = c;
            if (winner(table) == 'x') {
                drawtable(table);
                System.out.println("x win!!");
                gameEnd = true;
                
                
            } else if (winner(table) == 'o') {
                drawtable(table);
                System.out.println("o win!!");
                gameEnd = true;
            } else {
                if (tableFull(table)) {
                    System.out.println("It's a tie!");
                    gameEnd = true;
                } else {
                    p1 = !p1;
                }
            }
            
            
        }

    }

    private static void drawtable(char[][] table) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    private static boolean tableFull(char[][] table) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static char winner(char[][] table) {
        for (int i = 0; i < table.length; i++) {
            boolean inARow = true;
            char value = table[i][0];
            if (value == '-') {
                inARow = false;
            } else {
                for (int j = 1; j < table[i].length; j++) {
                    if (table[i][j] != value) {
                        inARow = false;
                        break;
                    }
                }
            }
            if (inARow) {
                return value;
            }
        }
        for (int i = 0; i < table[0].length; i++) {
            boolean inACol = true;
            char value = table[0][i];
            if (value == '-') {
                inACol = false;
            } else {
                for (int j = 1; j < table.length; j++) {
                    if (table[j][i] != value) {
                        inACol = false;
                        break;
                    }
                }
            }
            if (inACol) {
                return value;
            }
        }
        boolean inADiag1 = true;
        char value1 = table[0][0];
        if (value1 == '-') {
            inADiag1 = false;
        } else {
            for (int i = 1; i < table.length; i++) {
                if (table[i][i] != value1) {
                    inADiag1 = false;
                    break;
                }
            }
        }
        if (inADiag1) {
            return value1;
        }

        boolean inADiag2 = true;
        char value2 = table[0][table.length - 1];

        if (value2 == '-') {
            inADiag2 = false;
        } else {
            for (int i = 1; i < table.length; i++) {
                if (table[i][table.length - 1 - i] != value2) {
                    inADiag2 = false;
                    break;
                }
            }
        }
        if (inADiag2) {
            return value2;
        }
        return ' ';

    }
    
}
